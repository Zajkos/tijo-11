#Adapter (wzorzec projektowy)

**Adapter** - strukturalny wzorzec projektowy, którego celem jest 
umożliwienie współpracy dwóm klasom o niekompatybilnych interfejsach.
Adapter przekształca interfejs jednej z klas na interfejs drugiej klasy. 
Innym zadaniem omawianego wzorca jest opakowanie istniejącego interfejsu w nowy.
 
##Problem
 
Wzorzec adaptera stosowany jest najczęściej w przypadku, gdy wykorzystanie istniejącej 
klasy jest niemożliwe ze względu na jej niekompatybilny interfejs. Drugim powodem użycia
może być chęć stworzenia klasy, która będzie współpracowała z klasami o nieokreślonych
interfejsach.
 
##Struktura wzorca

Istnieją dwa warianty wzorca Adapter:

* klasowy,

![Klasowy](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/AdapterV1_classes_pl.svg/1920px-AdapterV1_classes_pl.svg.png)

* obiektowy
 
![Obiektowy](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/AdapterV2_classes_pl.svg/1920px-AdapterV2_classes_pl.svg.png)

_Różnią się one nieznacznie budową oraz właściwościami. Do stworzenia adaptera 
klasowego wykorzystywane jest wielokrotne dziedziczenie. Klasa adaptera dziedziczy
prywatnie po klasie adaptowanej oraz publicznie implementuje interfejs klienta.
W przypadku tego adaptera wywołanie funkcji jest przekierowywane do bazowej klasy
adaptowanej._

_W przypadku adaptera obiektowego klasa adaptera dziedziczy interfejs, którym posługuje 
się klient oraz zawiera w sobie klasę adaptowaną. Rozwiązanie takie umożliwia oddzielenie 
klasy klienta od klasy adaptowanej. Komplikuje to proces przekazywania żądania: klient 
wysyła je do adaptera wywołując jedną z jego metod. Następnie adapter konwertuje 
wywołanie na jedno bądź kilka wywołań i kieruje je do obiektu/obiektów adaptowanych. 
Te przekazują wyniki działania bezpośrednio do klienta._

##Adapter dwukierunkowy

Zadaniem adaptera dwukierunkowego jest adaptowanie interfejsów klienta oraz adaptowanego.
Dzięki takiemu rozwiązaniu każda z klas może pełnić zarówno funkcję klienta jak 
i adaptowanego. Ten typ adaptera można zaimplementować tylko za pomocą wielokrotnego 
dziedziczenia.

##Konsekwencje stosowania

Konsekwencje stosowania wzorca są różne w zależności od tego, z jakim typem mamy do czynienia.
W przypadku typu klasowego są to:

* brak możliwości adaptowania klasy wraz z jej podklasami,
* możliwość przeładowania metod obiektu adaptowanego.

Do konsekwencji stosowania adaptera obiektowego należą:

* możliwość adaptacji klasy wraz z jej podklasami (związane jest to z wykorzystaniem składania obiektów),
* możliwość dodawania nowej funkcjonalności,
* brak możliwości przeładowania metod obiektu adaptowanego.

W obu przypadkach należy liczyć się z narzutem wydajnościowym — tym większym,
 im większa jest niekompatybilność interfejsów.


Źródła: 

[Wikipedia](https://pl.wikipedia.org/wiki/Adapter_(wzorzec_projektowy))

[Tutorialspoint](https://www.tutorialspoint.com/design_pattern/adapter_pattern.htm) 


